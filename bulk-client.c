#include <assert.h>
#include <stdio.h>
#include <margo.h>
#include "bulk-types.h"

int32_t values[10] = { 1,4,2,5,6,3,5,3,2,5 };

int main(int argc, char** argv)
{
    if(argc != 2) {
        fprintf(stderr,"Usage: %s <server address>\n", argv[0]);
        exit(0);
    }

    margo_instance_id mid = margo_init("tcp", MARGO_CLIENT_MODE, 0, 0);

    hg_id_t sum_rpc_id = MARGO_REGISTER(mid, "sum", sum_in_t, sum_out_t, NULL);

    hg_addr_t svr_addr;
    margo_addr_lookup(mid, argv[1], &svr_addr);

    int i;
    sum_in_t args;
    for(i=0; i<4; i++) {

        hg_handle_t h;
        margo_create(mid, svr_addr, sum_rpc_id, &h);

        hg_size_t size = 10*sizeof(int32_t);

        hg_bulk_t local_bulk;
        margo_bulk_create(mid, 1, (void**)&values, &size, HG_BULK_READ_ONLY, &local_bulk);

        args.n = 10;
        args.bulk = local_bulk;

        margo_forward(h, &args);

        sum_out_t resp;
        margo_get_output(h, &resp);

        printf("Got response: %d\n", resp.ret);

        margo_free_output(h,&resp);
        margo_destroy(h);

        margo_bulk_free(local_bulk);
    }

    margo_addr_free(mid, svr_addr);

    margo_finalize(mid);

    return 0;
}
