CFLAGS = -ggdb3 -O0

TARGETS = hello-client hello-server bulk-client bulk-server

all: $(TARGETS)


hello-client: hello-client.c
	cc -o hello-client hello-client.c $(shell pkg-config --cflags --libs margo) $(CFLAGS) 

hello-server: hello-server.c
	cc -o hello-server hello-server.c $(shell pkg-config --cflags --libs margo) $(CFLAGS)

bulk-client: bulk-types.h bulk-client.c
	cc -o bulk-client bulk-client.c $(shell pkg-config --cflags --libs margo) $(CFLAGS) 

bulk-server: bulk-types.h bulk-server.c
	cc -o bulk-server bulk-server.c $(shell pkg-config --cflags --libs margo) $(CFLAGS)

clean:
	rm -f $(TARGETS)
